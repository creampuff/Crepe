Crepe
------------------
Chromium based web UI engine for Creampuff UIKit.
And it will also support other platforms such as OSX, Windows.


### Features

- Container Build System

- Cross Platform

- APIs accessing DOM


### Platforms
|          OS          |   Status    |
|:--------------------:|:-----------:|
| Creampuff (Linux)    |  Not work   |
| OSX                  |  Not work   |
| Windows              |  Not work   |
| Android              |  Not work   |


### Bindings
|       Language        |   Available   |
|:---------------------:|:-------------:|
|     Swift             |      Full     |
|     Go                |      Full     |
|     Rust              |     Level1    |
|     JavaScript        |     Level2    |
|     Java              |   Unavailable |
